package no.ntnu.imt3281.project1;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The dropdown menu on top of the application.
 * @author Maciek
 *
 */
public class GUIMenu extends JMenuBar
{
	private static final long serialVersionUID = 1L;
	
	private JMenu JMenu;
	private JMenu JMenu2;
	private JMenu JMenu3;
	
	private JMenuItem New;
	private JMenuItem Load;
	private JMenuItem Save;
	private JMenuItem SaveAs;
	private JMenuItem Preview;
	private JMenuItem GenerateJavaCode;
	private JMenuItem Exit;
	private JMenuItem NewRow;
	private JMenuItem Preferences;
	private JMenuItem Help;
	private JMenuItem About;	
	
	
	/**
	 * Initialization of the menu, each option belongs to a JMenu, and contains custom text, some contain a picture,
	 * and all contain an action listener that responds to clicks.
	 * @author Maciek
	 */
	public GUIMenu() {
		
		//File menu
		JMenu = new JMenu(Lang.getLang().getString("menu.file"));
		
		this.New = new JMenuItem(Lang.getLang().getString("menu.file.new"));
		ImageIcon NewImage = new ImageIcon(this.getClass().getResource("/New.gif"));
		New.setIcon(NewImage);
		JMenu.add(this.New);
		this.New.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent NewAction) {
		    	MenuToolbarCommon.CommonNew();
		    }
		});
		
		this.Load = new JMenuItem(Lang.getLang().getString("menu.file.load"));
		ImageIcon LoadImage = new ImageIcon(this.getClass().getResource("/OpenDoc.gif"));
		Load.setIcon(LoadImage);
		JMenu.add(this.Load);
		this.Load.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent LoadAction) {
		    	MenuToolbarCommon.CommonLoad();
		    }
		});
		
		this.Save = new JMenuItem(Lang.getLang().getString("menu.file.save"));
		ImageIcon SaveImage = new ImageIcon(this.getClass().getResource("/Save.gif"));
		Save.setIcon(SaveImage);
		JMenu.add(this.Save);
		this.Save.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent SaveAction) {
		    	MenuToolbarCommon.CommonSave();
		    }
		});
		
		this.SaveAs = new JMenuItem(Lang.getLang().getString("menu.file.saveas"));
		ImageIcon SaveAsImage = new ImageIcon(this.getClass().getResource("/Save.gif"));
		SaveAs.setIcon(SaveAsImage);
		JMenu.add(this.SaveAs);
		this.SaveAs.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent SaveAction) {
		    	MenuToolbarCommon.CommonSaveAs();
		    }
		});
		
		this.Preview = new JMenuItem(Lang.getLang().getString("menu.file.preview"));
        //No icon
		JMenu.add(this.Preview);
		this.Preview.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent PreviewAction) {
		    	MenuToolbarCommon.CommonPreview();
		    }
		});
		
		this.GenerateJavaCode = new JMenuItem(Lang.getLang().getString("menu.file.generatejavacode"));
		ImageIcon GenerateJavaCodeImage = new ImageIcon(this.getClass().getResource("/SaveJava.gif"));
		GenerateJavaCode.setIcon(GenerateJavaCodeImage);
		JMenu.add(this.GenerateJavaCode);
		this.GenerateJavaCode.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent GenerateJavaCodeAction) {
		    	MenuToolbarCommon.CommonPreview();
		    }
		});
		
		this.Exit = new JMenuItem(Lang.getLang().getString("menu.file.exit"));
		//No icon
		JMenu.add(this.Exit);
		this.Exit.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent GenerateJavaCodeAction) {
		    	MenuToolbarCommon.CommonExit();
		    }
		});
				
		
		//Edit items in application menu
		JMenu2 = new JMenu(Lang.getLang().getString("menu.edit"));
		
		this.NewRow = new JMenuItem(Lang.getLang().getString("menu.edit.newrow"));
		ImageIcon NewRowImage = new ImageIcon(this.getClass().getResource("/NewRow.gif"));
		NewRow.setIcon(NewRowImage);
		JMenu2.add(this.NewRow);
		this.NewRow.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent NewRowAction) {
		    	MenuToolbarCommon.CommonNewRow();
		    }
		});
		
		this.Preferences = new JMenuItem(Lang.getLang().getString("menu.edit.preferences"));
		//No icon
		JMenu2.add(this.Preferences);
		this.Preferences.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent GenerateJavaCodeAction) {
		    	MenuToolbarCommon.CommonPreferences();
		    }
		});
				
		
		//Help and about, information about the application
		JMenu3 = new JMenu(Lang.getLang().getString("menu.help"));
		
		this.Help = new JMenuItem(Lang.getLang().getString("menu.help.help"));
		ImageIcon HelpImage = new ImageIcon(this.getClass().getResource("/Help.gif"));
		Help.setIcon(HelpImage);
		JMenu3.add(this.Help);
		this.Help.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent HelpAction) {
		    	MenuToolbarCommon.CommonHelp();
		    }
		}); 
		
		this.About = new JMenuItem(Lang.getLang().getString("menu.help.about"));
		//No icon
		JMenu3.add(this.About);
		this.About.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent GenerateJavaCodeAction) {
		    	MenuToolbarCommon.CommonAbout();
		    }
		});
		
		this.add(JMenu);
		this.add(JMenu2);
		this.add(JMenu3);
	}	
	
	/**
	 * Initialization for the toolbar, each button has an object created, added to the toolbar
	 * 	and added a action listener for clicks.
	 * Code for what happens on click is handled by the MenuToolbarCommon class, because of functional
	 * 	duplicity between the menu and the toolbar (most of what both do share code)
	 * @author Maciek
	 */
	public void regenerateAfterLanguageChange() {
		this.JMenu.setText(Lang.getLang().getString("menu.file"));
		this.JMenu2.setText(Lang.getLang().getString("menu.edit"));
		this.JMenu3.setText(Lang.getLang().getString("menu.help"));
		
		this.New.setText(Lang.getLang().getString("menu.file.new"));
		this.Load.setText(Lang.getLang().getString("menu.file.load"));
		this.Save.setText(Lang.getLang().getString("menu.file.save"));
		this.SaveAs.setText(Lang.getLang().getString("menu.file.saveas"));
		this.Preview.setText(Lang.getLang().getString("menu.file.preview"));
		this.GenerateJavaCode.setText(Lang.getLang().getString("menu.file.generatejavacode"));
		this.Exit.setText(Lang.getLang().getString("menu.file.exit"));
		this.NewRow.setText(Lang.getLang().getString("menu.edit.newrow"));
		this.Preferences.setText(Lang.getLang().getString("menu.edit.preferences"));
		this.Help.setText(Lang.getLang().getString("menu.help.help"));
		this.About.setText(Lang.getLang().getString("menu.help.about"));
	}
}