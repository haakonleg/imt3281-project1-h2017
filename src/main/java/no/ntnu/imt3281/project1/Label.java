package no.ntnu.imt3281.project1;

/**
 * The class Label extends BaseComponent and represents a javax.swing.JLabel object
 * @author Hakkon
 *
 */

public class Label extends BaseComponent
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor empty because there are no special attributes to set
	 */
	public Label()
	{
		
	}
	
	/**
	 * Constructor of Label creates a new Button based upon an existing BaseComponent object
	 * @param component BaseComponent object to copy
	 */
	public Label(BaseComponent component)
	{
		super(component);
	}
	
	/**
	 * This method returns the string for the definition of this swing component in Java code
	 * @return String java code definition
	 */
	 @Override
	 public String getDefinition()
	 {
		 return "\tJLabel " + this.variableName + " = new JLabel(\"" + this.text + "\");\n";
	 }
}