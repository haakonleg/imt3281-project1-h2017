package no.ntnu.imt3281.project1;

/**
 * The class TextField extends BaseComponent and represents a javax.swing.JTextField object
 * @author Hakkon
 *
 */

public class TextField extends BaseComponent
{
	private static final long serialVersionUID = 1L;
	
	private int width;
	
	/**
	 * Constructor creates a new TextField object and initializes the attributes to default values
	 */
	public TextField()
	{
		this.width = 0;
	}
	
	/**
	 * Constructor creates a new TextField object based upon another BaseComponent object and copies the attributes
	 * to the new TextField object
	 * @param component BaseComponent object to copy
	 */
	public TextField(BaseComponent component) {
		super(component);
		// If component is another TextField object copy its attributes
		if (component instanceof TextField) {
			this.width = ((TextField)component).getWidth();
		} else {
			this.width = 0;
		}
	}
	
	/**
	 * This method returns the width attribute of this component
	 * @return Int the width attribute of this object
	 */
	public int getWidth()
	{
		return this.width;
	}
	
	/**
	 * This method sets the width attribute of this component
	 * @param w Int the integer to set the width attribute to
	 */
	public void setWidth(int w)
	{
		this.width = w;
	}
	
	/**
	 * This method returns the string for the definition of this swing component in Java code
	 * @return String java code definition
	 */
	@Override
	public String getDefinition()
	{
		return "\tJTextField " + this.variableName + " = new JTextField(\""+ this.text + "\", " + this.width + ");\n";
	}
	
	/**
	 * This method returns a JFrame window which contains the GUI to change
	 * special attributes of this component such as width.
	 * @return JFrame GUI which can be used to change special attributes
	 */
	@Override
	public java.awt.Component getSpecialEditor()
	{
		return new SpecialEditorTextField(this);
	}
}