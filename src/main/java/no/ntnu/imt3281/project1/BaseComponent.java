package no.ntnu.imt3281.project1;

/**
 * This class serves as the superclass of all components in the data model such as
 * Button, TextArea etc, and it contains all the common attributes of these components.
 *
 */

public class BaseComponent implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;
	
	// Static int which keeps track of how many objects of BaseComponent are created
	// incremented by one after a new object is created
	static int nextComponentID = 1;
	
	protected String variableName;
	protected String text;
	protected int row;
	protected int col;
	protected int rows;
	protected int cols;
	protected int anchor;
	protected int fill;
	
	/**
	 * Constructor creates a new BaseComponent without any parameters and
	 * initializes all attributes to default values.
	 */
	public BaseComponent()
	{
		this.variableName = "component" + Integer.toString(nextComponentID);
		this.text = "";
		this.row = 1;
		this.col = 1;
		this.cols = 1;
		this.rows = 1;
		this.anchor = java.awt.GridBagConstraints.CENTER;
		this.fill = java.awt.GridBagConstraints.NONE;
		nextComponentID++;
	}
	
	/**
	 * Constructor creates a new BaseComponent based upon an existing
	 * BaseComponent object by copying it's attributes to the new one.
	 * @param component The BaseComponent object to copy
	 */
	public BaseComponent(BaseComponent component)
	{
		this.variableName = component.variableName;
		this.text = component.text;
		this.row = component.row;
		this.col = component.col;
		this.cols = component.cols;
		this.rows = component.rows;
		this.anchor = component.anchor;
		this.fill = component.fill;
	}
	
	/**
	 * The method getDefinition is overridden in each subclass of BaseComponent
	 * and returns the java code to define the component
	 * @return String Returns the Java code
	 */
	public String getDefinition()
	{
		return null;
	}
	
	/**
	 * Method getLayoutCode constructs the layout code needed by GridBagLayout which
	 * is used in the JPanel constructor of the generated code
	 * @return String with Java code
	 */
	public String getLayoutCode()
	{
		String code = 
				"\t\tgbc.gridx = " + Integer.toString(this.col) + ";\n" +
				"\t\tgbc.gridy = " + Integer.toString(this.row) + ";\n" +
				"\t\tgbc.gridwidth = " + Integer.toString(this.cols) + ";\n" +
				"\t\tgbc.gridheight = " + Integer.toString(this.rows) + ";\n" +
				"\t\tgbc.anchor = " + Integer.toString(this.anchor) + ";\n" +
				"\t\tgbc.fill = " + Integer.toString(this.fill) + ";\n" +
				"\t\tlayout.setConstraints(" + this.variableName + ", gbc);\n" +
				"\t\tadd(" + this.variableName + ");\n";
		return code;
	}
	
	/**
	 * This method returns the name of the component
	 * @return String This returns name of the component
	 */
	public String getVariableName()
	{
		return this.variableName;
	}
	
	/**
	 * This method sets the variable name of the component
	 * @param s String to use as variable name
	 */
	public void setVariableName(String s)
	{
		this.variableName = s;
	}
	
	/**
	 * This method returns the text attribute of the component
	 * @return String Component text
	 */
	public String getText()
	{
		return this.text;
	}
	
	/**
	 * This method sets the text attribute of the component
	 * @param s String to use as text
	 */
	public void setText(String s)
	{
		this.text = s;
	}
	
	/**
	 * This method returns the row attribute of the component
	 * @return Int row attribute
	 */
	public int getRow()
	{
		return this.row;
	}
	
	/**
	 * This method sets the row attribute of the component
	 * @param n Int to use as row number
	 */
	public void setRow(int n)
	{
		this.row = n;
	}
	
	/**
	 * This method returns the rows attribute of the component
	 * @return Int rows attribute
	 */
	public int getRows()
	{
		return this.rows;
	}
	
	/**
	 * This method sets the rows attribute of the component
	 * @param n Int number used as rows attribute
	 */
	public void setRows(int n)
	{
		this.rows = n;
	}
	
	/**
	 * This method returns the column attribute of the component
	 * @return Int the column attribute for this component
	 */
	public int getCol()
	{
		return this.col;
	}
	
	/**
	 * This function sets the column attribute of the component
	 * @param n Int number to set column attribute to
	 */
	public void setCol(int n)
	{
		this.col = n;
	}
	
	/**
	 * This function returns the columns attribute of the component
	 * @return Int the columns attribute for this component
	 */
	public int getCols()
	{
		return this.cols;
	}
	
	/**
	 * This method sets the columns attribute of this component
	 * @param n Int the number to set columns attribute to
	 */
	public void setCols(int n)
	{
		this.cols = n;
	}
	
	/**
	 * This method returns the anchor attribute of this component
	 * @return Int the anchor attribute for this object
	 */
	public int getAnchor()
	{
		return this.anchor;
	}
	
	/**
	 * This method sets the anchor attribute of this component
	 * @param n Int the anchor attribute to set for this object
	 */
	public void setAnchor(int n)
	{
		this.anchor = n;
	}
	
	/**
	 * This method returns the fill attribute of this component
	 * @return Int the fill attribute for this component
	 */
	public int getFill()
	{
		return this.fill;
	}
	
	/**
	 * This method sets the fill attribute of this component
	 * @param n Int the attribute to set for this object
	 */
	public void setFill(int n)
	{
		this.fill = n;
	}
	
	/**
	 * This method is overridden in TextField and TextArea components
	 * returns a special editor Component for the component
	 * @return Component Returns special editor
	 */
	public java.awt.Component getSpecialEditor()
	{
		return null;
	}
}