package no.ntnu.imt3281.project1;

import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/**
 * The bottom panel that displays status messages about the application.
 * @author Maciek
 *
 */
public class GUIStatusPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private static GUIStatusPanel statusPanel;
	private static JLabel status;
	
	static
	{
		statusPanel = new GUIStatusPanel();
		statusPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		status = new JLabel();
		status.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 10));
		statusPanel.add(status);
	}
	
	public static void setStatus(String message)
	{
		status.setText(message);
	}
	
	public static GUIStatusPanel getInstance()
	{
		return statusPanel;
	}
}