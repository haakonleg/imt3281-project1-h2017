package no.ntnu.imt3281.project1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 * The class GBLEDataModel provides the implementation for the table model of the application.
 * It extends AbstractTableModel and overrides nessecary methods so it can be used by a JTable object
 * It also contains other methods that is used in the table such as adding, removing, and moving components.
 * @author Hakkon
 *
 */

public class GBLEDataModel extends AbstractTableModel
{
	private static final long serialVersionUID = 1L;
	
	private Vector<BaseComponent> data;
	private String[] columnNames;
	
	/**
	 * Constructor for GBLEDataModel. Initializes the vector and strings
	 * used in the column names of the table.
	 */
	public GBLEDataModel()
	{
		data = new Vector<BaseComponent>();
		columnNames = new String[9];
		// Set column names
		for (int i = 0; i < columnNames.length; i++)
			columnNames[i] = Lang.getLang().getString("table.column" + Integer.toString(i+1));
	}

	/**
	 * Returns the number of columns in the table model
	 * @return int Number of columns
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/**
	 * Returns the number of rows in the table model
	 * @return int Number of rows
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}

	/**
	 * Method getValueAt will return the value at col(x,y) in the table
	 * in this model these values are the attributes of the components in the table.
	 * @param row The table row to select
	 * @param col The table coloumn to select
	 * @return Object of type String or Int 
	 */
	@Override
	public Object getValueAt(int row, int col) {
		BaseComponent c = this.data.elementAt(row);
		switch (col)
		{
			// Return type
			case 0:
				if (c instanceof TextField) return "JTextField";
				else if (c instanceof TextArea) return "JTextArea";
				else if (c instanceof Label) return "JLabel";
				else return "JButton";
			// Return name
			case 1:
				return c.getVariableName();
			// Return text
			case 2:
				return c.getText();
			// Return row
			case 3:
				return c.getRow();
			// Return column
			case 4:
				return c.getCol();
			// Return rows
			case 5:
				return c.getRows();
			// Return columns
			case 6:
				return c.getCols();
			// Return anchor
			case 7:
				return c.getAnchor();
			// Return fill
			case 8:
				return c.getFill();
			default:
				return null;
		}
	}
	
	/**
	 * This function returns the column name of the specified column
	 * @param n Integer with column number
	 * @return String name of column
	 */
	@Override
	public String getColumnName(int n) {
		return columnNames[n];
	}
	
	@Override
	public Class getColumnClass(int n) {
		return getValueAt(0, n).getClass();
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
			return true;
	}
	
	/**
	 * Function setValueAt is used by JTable when the user edits contents in a table cell.
	 * It it responsible for correctly calling functions in BaseComponent to
	 * be able to set the attributes of the components. Uses a switch to
	 * tell which column is being modified.
	 * @param value Object of any type
	 * @param row Which row to select
	 * @param col Which col to select
	 */
	@Override
	public void setValueAt(Object value, int row, int col) {
		BaseComponent c = this.data.elementAt(row);
		switch (col)
		{
			// Set type
			case 0:
				BaseComponent temp;
				switch((String)value)
				{
					case "JTextField":
						temp = new TextField(c);
						this.data.setElementAt(temp, row);
						break;
					case "JTextArea":
						temp = new TextArea(c);
						this.data.setElementAt(temp, row);
						break;
					case "JLabel":
						temp = new Label(c);
						this.data.setElementAt(temp, row);
						break;
					case "JButton":
						temp = new Button(c);
						this.data.setElementAt(temp, row);
						break;
				}
				break;
			// Set variable name
			case 1:
				c.setVariableName((String)value);
				break;
			// Set text
			case 2:
				c.setText((String)value);
				break;
			// Set row
			case 3:
				c.setRow((int)value);
				break;
			// Set column
			case 4:
				c.setCol((int)value);
				break;
			// Set rows
			case 5:
				c.setRows((int)value);
				break;
			// Set columns
			case 6:
				c.setCols((int)value);
				break;
			// Set anchor
			case 7:
				c.setAnchor((int)value);
				break;
			// Set fill
			case 8:
				c.setFill((int)value);
				break;
			default:
				break;
		}
		this.fireTableCellUpdated(row, col);
	}
	
	public String getDefinitions()
	{
		String buffer = "";
		for(int i = 0; i < this.data.size(); i++)
		{
			buffer += this.data.elementAt(i).getDefinition();
		}
		return buffer;
	}
	
	public String getLayoutCode()
	{
		String buffer = "";
		for(int i = 0; i < this.data.size(); i++)
		{
			buffer += this.data.elementAt(i).getLayoutCode() + "\n";
		}
		return buffer;
	}
	
	/**
	 * Saves the data vector of BaseComponents to file PRE-selected by user
	 * @param os Initialized FileOutputStream with location where data is to be saved
	 */
	public void save(OutputStream os)
	{
		try {
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(data);
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads in and replaces the data vector of BaseComponents with data from file PRE-selected by user
	 * @param is Initialized FileInputStream with location where data is to be loaded from
	 */
	public void load(InputStream is)
	{
		try {
			ObjectInputStream ois = new ObjectInputStream(is);
			data = (Vector<BaseComponent>) ois.readObject();
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This function removes every component in the table model. Every BaseComponent
	 * object in the data vector will be removed. It then notifies listeners that
	 * these table rows have been deleted.
	 */
	public void clear()
	{
		int len = this.data.size();
		this.data.removeAllElements();
		this.fireTableRowsDeleted(0, len);
		BaseComponent.nextComponentID = 1;
		GUIStatusPanel.setStatus("Table reset");
	}
	
	/**
	 * This function adds a component in the table model. It inserts a BaseComponent
	 * object to the end of the vector and notifies listeners that a new table row
	 * has been added.
	 * @param component BaseComponent object to add
	 */
	public void addComponent(BaseComponent component)
	{
		this.data.add(component);
		this.fireTableRowsInserted(this.data.size() - 1, this.data.size() - 1);
		GUIStatusPanel.setStatus("Added new component " + component.getClass());
	}
	
	/**
	 * This function removes a component in a specified row of the table.
	 * It removes the object in the data vector and then notifies listeners
	 * that the row has been deleted.
	 * @param n Number of the row to remove
	 */
	public void removeComponent(int n)
	{
		GUIStatusPanel.setStatus("Deleted " + this.data.elementAt(n).getVariableName());
		this.data.remove(n);
		this.fireTableRowsDeleted(n, n);
	}
	
	/**
	 * This function removes a specified component in the table. It
	 * removes the object from the data vector and notifies listeners
	 * that this row has been deleted from the table.
	 * @param component Object to remove
	 */
	public void removeComponent(BaseComponent component)
	{
		int index = this.data.indexOf(component);
		GUIStatusPanel.setStatus("Deleted " + this.data.elementAt(index).getVariableName());
		this.data.remove(component);
		this.fireTableRowsDeleted(index, index);
		GUIStatusPanel.getInstance();
	}
	
	/**
	 * This function moves a component in the table one row up.
	 * It does this by deleting and then inserting the element at position n to n-1
	 * in the data vector.
	 * @param n Number of component to move up
	 */
	public void moveComponentUp(int n)
	{
		if (n > 0)
		{
			// Save component to temp and remove
			BaseComponent temp = this.data.elementAt(n);
			this.removeComponent(n);
			// Insert temp component to n-1
			this.data.insertElementAt(temp, n-1);
			this.fireTableRowsInserted(n-1, n);
		}
	}
	
	/**
	 * This function moves a component in the table one row down.
	 * It does this by deleting and then inserting the element at position n to n+1
	 * in the data vector.
	 * @param n Number of component to move up
	 */
	public void moveComponentDown(int n)
	{
		if(n < this.data.size()-1 && n >= 0)
		{
			// Save component to temp and remove
			BaseComponent temp = this.data.elementAt(n);
			this.removeComponent(n);
			// Insert temp component to n+1
			this.data.insertElementAt(temp, n+1);
			this.fireTableRowsInserted(n+1, n);
		}
	}
	
	/**
	 * This function returns the component object of specified row
	 * It is nessecary to be able to call function getSpecialEditor
	 * in the BaseComponent objects
	 * @param n The row number for the component to return
	 * @return BaseComponent The component object
	 */
	public BaseComponent getComponent(int n)
	{
		return data.elementAt(n);
	}
	
	/**
	 * After changing the language in the application,
	 * the header of the table needs to be translated to the new language.
	 * This function sets the header to new values, and then sends signal
	 * that the structure of the table changed, causing a redraw of the table.
	 * @author Maciek
	 */
	public void regenerateAfterLanguageChange() {
		for (int i = 0; i < columnNames.length; i++)
			columnNames[i] = Lang.getLang().getString("table.column" + Integer.toString(i+1));
		this.fireTableStructureChanged();
	}
}