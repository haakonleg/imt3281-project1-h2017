package no.ntnu.imt3281.project1;

import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 * GBLEMain is the class responsible for creating the window/JFrame for
 * the GUI and all its panels. It also contains the main function of the application
 * which initializes the JFrame
 * @author Everyone
 *
 */

public class GBLEMain extends JFrame
{
	private static final long serialVersionUID = 1L;

	// Data model
	private static GBLEDataModel model = new GBLEDataModel();
	
	// Define components/panels
	private GUIToolbar toolbar;
	private GUIMenu menu;
	private GUITablePanel tablePanel;
	/**
	 * Constructor of GBLEMain
	 * Adds the panels and components to the JFrame instance and
	 * defines the nessecary parameters of the window frame
	 */
	public GBLEMain()
	{
		super(Lang.getLang().getString("frame.title"));
		this.setLayout(new BorderLayout());

		// Add panels
		toolbar = new GUIToolbar();
		menu = new GUIMenu();
		tablePanel = new GUITablePanel(model);
		MenuToolbarCommon.SetVariables(tablePanel, model, this);
		this.add(toolbar, BorderLayout.PAGE_START);
		this.add(tablePanel, BorderLayout.CENTER);
		this.add(GUIStatusPanel.getInstance(), BorderLayout.SOUTH);
		this.setJMenuBar(menu);
		
		// Add some components for testing
		model.addComponent(new TextArea());
		model.addComponent(new Label());
		model.addComponent(new TextField());
		
		// Set status message
		GUIStatusPanel.setStatus("Ready");
		
		// Window size, listener, and close operation
		this.setLocationRelativeTo(null);
		this.setSize(800, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Initialization for the toolbar, each button has an object created, added to the toolbar
	 * 	and added a action listener for clicks.
	 * Code for what happens on click is handled by the MenuToolbarCommon class, because of functional
	 * 	duplicity between the menu and the toolbar (most of what both do share code)
	 * @author Maciek
	 */
	public void regenerateAfterLanguageChange() {
		toolbar.regenerateAfterLanguageChange();
		menu.regenerateAfterLanguageChange();
		model.regenerateAfterLanguageChange();
		tablePanel.regenerateAfterLanguageChange();
	}
	
	public static void main(String[] args) {
		GBLEMain main = new GBLEMain();
		main.setVisible(true);
	}
	
}