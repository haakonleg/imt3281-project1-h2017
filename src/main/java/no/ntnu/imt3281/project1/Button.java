package no.ntnu.imt3281.project1;

/**
 * The class Button extends BaseComponent and represents a javax.swing.JButton object
 * @author Hakkon
 *
 */
public class Button extends BaseComponent
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor empty because there are no special attributes to set
	 */
	public Button()
	{
		
	}
	
	/**
	 * Constructor of Button creates a new Button based upon an existing BaseComponent object
	 * @param component BaseComponent object to copy
	 */
	public Button(BaseComponent component)
	{
		super(component);
	}
	
	/**
	 * This method returns the string for the definition of this swing component in Java code
	 * @return String java code definition
	 */
	 @Override
	 public String getDefinition()
	 {
		 return "\tJButton " + this.variableName + " = new JButton(\"" + this.text + "\");\n";
	 }
}