package no.ntnu.imt3281.project1;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * The class Lang takes care of loading the language resources
 */

public class Lang
{
	private static ResourceBundle language;
	private static Locale currentLocale;
	
	static {
		currentLocale = Locale.getDefault();
		try {
			language = ResourceBundle.getBundle("editor", currentLocale);
		} catch(MissingResourceException e) {
			language = ResourceBundle.getBundle("editor", new Locale("en", "US"));
		}
	}
	
	/**
	 * This is the main method of Lang which other classes
	 * use to fetch language strings from the language resource
	 * @return ResourceBundle object
	 */
	public static ResourceBundle getLang() {
		return language;
	}
	
	/**
	 * This function selects the language of the application by changing the locale
	 * It then loads the ResourceBundle for the specified language into the Lang class
	 * @param lang The language to select, can be "en" or "no"
	 */
	public static void setLang(String lang) {
		switch(lang)
		{
			case "en":
				currentLocale = new Locale("en", "US");
				break;
			case "no":
				currentLocale = new Locale("nb", "NO");
				break;
			default:
				System.out.print("Error: unknown language " + lang);
				currentLocale = new Locale("en", "US");
				break;
		}
		language = ResourceBundle.getBundle("editor", currentLocale);
	}
}