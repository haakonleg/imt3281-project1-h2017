package no.ntnu.imt3281.project1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

/**
 * This class extends JToolBar and implements the toolbar menu for the GUI
 *
 */

public class GUIToolbar extends JToolBar
{
	private static final long serialVersionUID = 1L;
	
	private JButton New;
	private JButton Load;
	private JButton Save;
	private JButton Preview;
	private JButton GenerateJavaCode;
	private JButton NewRow;
	private JButton MoveUp;
	private JButton MoveDown;
	private JButton Help;
	
	
	/**
	 * Initialization for the toolbar, each button has an object created, added to the toolbar
	 * 	and added a action listener for clicks.
	 * Code for what happens on click is handled by the MenuToolbarCommon class, because of functional
	 * 	duplicity between the menu and the toolbar (most of what both do share code)
	 * @author Maciek
	 */
	public GUIToolbar() { 
		
	    this.New = new JButton(new ImageIcon(this.getClass().getResource("/New.gif")));
	    this.New.setToolTipText(Lang.getLang().getString("menu.file.new"));
	    this.add(this.New);
		this.New.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent NewAction) {
		    	MenuToolbarCommon.CommonNew();
		    }
		});
		
	    this.Load = new JButton(new ImageIcon(this.getClass().getResource("/OpenDoc.gif")));
	    this.Load.setToolTipText(Lang.getLang().getString("menu.file.load"));
	    this.add(this.Load);
		this.Load.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent LoadAction) {
		    	MenuToolbarCommon.CommonLoad();
		    }
		});

	    this.Save = new JButton(new ImageIcon(this.getClass().getResource("/Save.gif")));
	    this.Save.setToolTipText(Lang.getLang().getString("menu.file.save"));
	    this.add(this.Save);
		this.Save.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent SaveAction) {
		    	MenuToolbarCommon.CommonSave();
		    }
		});
		
	    this.Preview = new JButton(new ImageIcon(this.getClass().getResource("/ExecuteProject.gif")));
	    this.Preview.setToolTipText(Lang.getLang().getString("menu.file.preview"));
	    this.add(this.Preview);
		this.Preview.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent PreviewAction) {
		    	MenuToolbarCommon.CommonPreview();
		    }
		});
	    
	    this.GenerateJavaCode = new JButton(new ImageIcon(this.getClass().getResource("/SaveJava.gif")));
	    this.GenerateJavaCode.setToolTipText(Lang.getLang().getString("menu.file.generatejavacode"));
	    this.add(this.GenerateJavaCode);
		this.GenerateJavaCode.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent GenerateJavaCodeAction) {
		    	MenuToolbarCommon.CommonPreview();
		    }
		});
	
	    this.NewRow = new JButton(new ImageIcon(this.getClass().getResource("/NewRow.gif")));
	    this.NewRow.setToolTipText(Lang.getLang().getString("menu.edit.newrow"));
	    this.add(this.NewRow);
		this.NewRow.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent NewRowAction) {
		    	MenuToolbarCommon.CommonNewRow();
		    }
		});
	
	    this.MoveUp = new JButton(new ImageIcon(this.getClass().getResource("/MoveRowUp.gif")));
	    this.MoveUp.setToolTipText(Lang.getLang().getString("table.popup.moveup"));
	    this.add(this.MoveUp);
		this.MoveUp.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent MoveUpAction) {
		    	MenuToolbarCommon.CommonMoveUp();
		    }
		});
	
	    this.MoveDown = new JButton(new ImageIcon(this.getClass().getResource("/MoveRowDown.gif")));
	    this.MoveDown.setToolTipText(Lang.getLang().getString("table.popup.movedown"));
	    this.add(this.MoveDown);
		this.MoveDown.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent MoveDownAction) {
		    	MenuToolbarCommon.CommonMoveDown();
		    }
		});
	
	    this.Help = new JButton(new ImageIcon(this.getClass().getResource("/Help.gif")));
	    this.Help.setToolTipText(Lang.getLang().getString("menu.help.help"));
	    this.add(this.Help);
		this.Help.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent HelpAction) {
		    	MenuToolbarCommon.CommonHelp();
		    }
		}); 
    }
	
	/**
	 * After changing the language, text needs to be updated
	 * @author Maciek
	 */
	public void regenerateAfterLanguageChange() {
	    this.New.setToolTipText(Lang.getLang().getString("menu.file.new"));
	    this.Load.setToolTipText(Lang.getLang().getString("menu.file.load"));
	    this.Save.setToolTipText(Lang.getLang().getString("menu.file.save"));
	    this.Preview.setToolTipText(Lang.getLang().getString("menu.file.preview"));
	    this.GenerateJavaCode.setToolTipText(Lang.getLang().getString("menu.file.generatejavacode"));
	    this.NewRow.setToolTipText(Lang.getLang().getString("menu.edit.newrow"));
	    this.NewRow.setToolTipText(Lang.getLang().getString("menu.edit.newrow"));
	    this.MoveUp.setToolTipText(Lang.getLang().getString("table.popup.moveup"));
	    this.MoveDown.setToolTipText(Lang.getLang().getString("table.popup.movedown"));
	    this.Help.setToolTipText(Lang.getLang().getString("menu.help.help"));
	}
}
