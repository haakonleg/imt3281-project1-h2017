package no.ntnu.imt3281.project1;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * This class contains a JFrame object which is used as the GUI to change
 * special attributes of the TextField component and is returned by itself in
 * the getSpecialEditor() method
 * 
 */


public class SpecialEditorTextField extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	private TextField textField;
	
	private JSpinner spinnerWidth;
	private JButton okButton;
	
	/**
	 * Constructor is responsible for initializing JFrame attributes and
	 * initializing and adding components to the JFrame layout
	 * @param object TextField object which is to be modified
	 */
	public SpecialEditorTextField(TextField object)
	{
		this.textField = object;
		
		this.setTitle(Lang.getLang().getString("specialeditor.title"));
		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		this.setResizable(false);
		this.getRootPane().setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		SpinnerNumberModel spinnerModel = 
				new SpinnerNumberModel(textField.getWidth(), 0, 100, 1);
		
		spinnerWidth = new JSpinner(spinnerModel);
		
		okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.setWidth((int)spinnerWidth.getValue());
				dispose();
			}
		});
		
		add(new JLabel(Lang.getLang().getString("specialeditor.width")));
		add(spinnerWidth);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(okButton);
		setSize(200, 80);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
	}
}