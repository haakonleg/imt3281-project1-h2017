package no.ntnu.imt3281.project1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * This class is a collection of functions used by the toolbar and menu 
 * @author Horcon
 *
 */
public class MenuToolbarCommon {

	private static File preselectedSaveFile = null;
	private static GUITablePanel TablePanel = null;
	private static GBLEDataModel DataModel = null;
	private static GBLEMain MainWindow = null;
	
	/**
	 * This function sets the local static variables to the data structure, table, and the main window
	 * so that the functions inside can operate on them without having to send everything as a parameter
	 * all the time.
	 * @param Tab GUITablePanel table object
	 * @param model	GBLEDataModel data structure object
	 * @param m - GBLEMain main window object
	 * @author Maciek
	 */
	public static void SetVariables(GUITablePanel Tab, GBLEDataModel model, GBLEMain m) {
		TablePanel = Tab;
		DataModel = model;
		MainWindow = m;
	}
	
	/**
	 * Common new file function, this function opens a window asking for confirmation,
	 * after which it wipes the data structure and the location of last saved file,
	 * giving the user a blank start to work from.
	 * @author Maciek
	 */
	public static void CommonNew() {
		String confirmText = Lang.getLang().getString("option.confirmNewText");
		String confirmWarning = Lang.getLang().getString("option.confirmNewWarning");
		String yes = Lang.getLang().getString("option.yes");
		String no = Lang.getLang().getString("option.no");
		Object[] options = {yes, no};
		
		
		int confirmButton = JOptionPane.showOptionDialog(null, confirmText, confirmWarning, 
				JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0] );
		if (confirmButton == JOptionPane.YES_OPTION) {
			DataModel.clear();
			preselectedSaveFile = null;
		}
	}
	
	/**
	 * Loads a data structure file from memory.
	 * @author Maciek
	 */
	public static void CommonLoad() {
    	//Create a new file pick window
		JFileChooser fileLoadWindow = new JFileChooser();
		fileLoadWindow.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = fileLoadWindow.showOpenDialog(null);
		
		//If user selected a file to load
		if (result == fileLoadWindow.APPROVE_OPTION) 
		{
			//Grab the file
			File fileLoad = fileLoadWindow.getSelectedFile();
			try {
				//Put it in a FileInputStream and send the stream to model, update main afterwards (since it has new data)
				FileInputStream fis = new FileInputStream(fileLoad);
				DataModel.load(fis);
				DataModel.fireTableDataChanged();
			} catch (FileNotFoundException e) {
				GUIStatusPanel.setStatus("File not found.");
			}
		}
	}
	
	/**
	 * Saves the data structure to a previously used file,
	 * if no file set, runs the CommonSaveAs()
	 */
	public static void CommonSave() {
		//If user selected a place to save a file previously
		if (preselectedSaveFile != null) 
		{
			//Create a File
			File fileSave = preselectedSaveFile;
			try {
				//Put it in a FileOutputStream and send the stream to model
				FileOutputStream fos = new FileOutputStream(fileSave);
				DataModel.save(fos);
			} catch (FileNotFoundException e) {
				GUIStatusPanel.setStatus("File not found.");
			}
		}
		else 
		{
			CommonSaveAs();
		}
	}
	
	/**
	 * Opens a window for the user to select where to save the data structure,
	 * then saves it there.
	 */
	public static void CommonSaveAs() {
		//Create a new file pick window
		JFileChooser fileSaveWindow = new JFileChooser();
		fileSaveWindow.setCurrentDirectory(new File(System.getProperty("user.home")));	//Window starts at users home directory
		int result = fileSaveWindow.showSaveDialog(null);
		
		//If user selected a place to save a file (and its name)
		if (result == fileSaveWindow.APPROVE_OPTION) 
		{
			//Create a File
			File fileSave = fileSaveWindow.getSelectedFile();
			preselectedSaveFile = fileSave;		//From now on, user can use save to save this file faster
			try {
				//Put it in a FileOutputStream and send the stream to model
				FileOutputStream fos = new FileOutputStream(fileSave);
				DataModel.save(fos);
			} catch (FileNotFoundException e) {
				GUIStatusPanel.setStatus("File not found.");
			}
		}
	}
	
	/**
	 * Saves the generated Java code, from the table that the user created, 
	 * to file, first opening a window for the user to choose where to save, then saving it there.
	 * Tries to create the file first, before trying to write to it.
	 * @author Maciek
	 */
	public static void CommonSaveJavaCode() {
		//Create a new file pick window
		JFileChooser saveWindow = new JFileChooser();
		saveWindow.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = saveWindow.showSaveDialog(null);
		
		//If user selected a place to save a file (and its name)
		if (result == JFileChooser.APPROVE_OPTION)
		{
			File file = saveWindow.getSelectedFile();
			String className = file.getName().substring(0, file.getName().lastIndexOf("."));
			try {
				file.createNewFile();		//Try to create a new file first
			} catch (IOException e) {
				GUIStatusPanel.setStatus("Error: unable to create file");
			}
			
			try {							//Then try to write the java code to it
				FileWriter fw = new FileWriter(saveWindow.getSelectedFile());
				fw.write(CommonGenerateJavaCode(DataModel, className));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				GUIStatusPanel.setStatus("Error: unable to write to file");
			}
		}
	}
	
	/**
	 * Opens a new window, where the user can view the code generated from the table.
	 * The user can save to file, clipboard, or close the window.
	 * @author Maciek
	 */
	public static void CommonPreview() {
		JFrame frame = new GUIPreview(DataModel, CommonGenerateJavaCode(DataModel, "MyClass"));
		frame.setVisible(true);
	}
	
	/**
	 * This function generates the Java code for a class that extends JPanel and contains all the components 
	 * added by the user to the data model and their definitions and layout codes. It also appends a 
	 * generated main function which creates a new JFrame containing the JPanel object and sets it to visible.
	 * @param className The name of the java class/file of the JPanel class in the generated code
	 * @param model The GBLEDataModel table model object
	 * @return String Returns the generated java code
	 */
	public static String CommonGenerateJavaCode(GBLEDataModel model, String className) {
		StringBuilder code = new StringBuilder(1024);
		code.append("// Generated with GBL Editor\n\n");
		
		// Import statements
		code.append("import javax.swing.*;\n")
			.append("import java.awt.*;\n\n")
		// New JPanel class and component definitions
			.append("public class " + className + " extends JPanel {\n")
			.append(model.getDefinitions() + "\n")
		// Constructor and layout code
			.append("\tpublic " + className + "() {\n")
			.append("\t\tGridBagLayout layout = new GridBagLayout();\n")
			.append("\t\tGridBagConstraints gbc = new GridBagConstraints();\n")
			.append("\t\tsetLayout(layout);\n\n")
			.append(model.getLayoutCode())
			.deleteCharAt(code.length()-1)
			.append("\t}\n\n")
		// Main function with JFrame
			.append("\tpublic static void main(String[] args) {\n")
			.append("\t\tJFrame frame = new JFrame();\n")
			.append("\t\t" + className + " panel = new " + className + "();\n")
			.append("\t\tframe.getContentPane().add(panel);\n")
			.append("\t\tframe.setSize(500, 500);\n")
			.append("\t\tframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n")
			.append("\t\tframe.setVisible(true);\n")
		// Closing brackets
			.append("\t}\n}");
		
		return code.toString();
	}
	
	/**
	 * Creates a new row on the table.
	 * @author Maciek
	 */
	public static void CommonNewRow() {
		DataModel.addComponent(new Label());
	}
	
	/**
	 * Moves a selected row up
	 * @author Maciek
	 */
	public static void CommonMoveUp() {
		DataModel.moveComponentUp(TablePanel.GetSelectedRow());
		
	}
	/**
	 * Moves a selected row down
	 * @author Maciek
	 */
	public static void CommonMoveDown() {
		DataModel.moveComponentDown(TablePanel.GetSelectedRow());
	}
	
	/**
	 * Displays a new window with a smaller version of the documentation
	 * @author Maciek
	 */
	public static void CommonHelp() {
		StringBuilder help = new StringBuilder(1024);
		
		help.append(Lang.getLang().getString("help.top") + "\n\n")
			.append(Lang.getLang().getString("help.new") + "\n")
			.append(Lang.getLang().getString("help.load") + "\n")
			.append(Lang.getLang().getString("help.save") + "\n")
			.append(Lang.getLang().getString("help.generate") + "\n")
			.append(Lang.getLang().getString("help.newrow") + "\n")
			.append(Lang.getLang().getString("help.help") + "\n\n")
			.append(Lang.getLang().getString("help.toolbarunique") + "\n")
			.append(Lang.getLang().getString("help.moverow") + "\n\n")
			.append(Lang.getLang().getString("help.menuunique") + "\n")
			.append(Lang.getLang().getString("help.preferences") + "\n")
			.append(Lang.getLang().getString("help.about") + "\n\n")
			.append(Lang.getLang().getString("help.tableunique") + "\n")
			.append(Lang.getLang().getString("help.rightclick") + "\n");
		
		JOptionPane.showMessageDialog(
				null, help.toString());
	}
	
	/**
	 * Lets the user change the language of the application.
	 * @author Maciek
	 */
	public static void CommonPreferences() {
		//Initial strings to display, and choice selection
		String message = Lang.getLang().getString("option.selectLanguage");
		String title = Lang.getLang().getString("menu.edit.preferences");
		Object[] selectionValues = { "English" , "Norsk" };
		
		//Current language in 2-letter string
		String currentLang = Lang.getLang().getLocale().getLanguage();
		
		//If the language is english, open an option pane with english as default selection
		//(and if norwegian is the active language, open it with norwegian as default
		if (currentLang == "en") {
			Object selection = JOptionPane.showInputDialog(null, 
					message, title, JOptionPane.QUESTION_MESSAGE, null, selectionValues, selectionValues[0]);
			if (selection != null) {	//If the user selected something
				currentLang = selection.toString();				
			}
			
		}
		else if(currentLang == "nb") {
			Object selection = JOptionPane.showInputDialog(null, 
					message, title, JOptionPane.QUESTION_MESSAGE, null, selectionValues, selectionValues[1]);
			if (selection != null) {	//If the user selected something
				currentLang = selection.toString();				
			}
		}
		
		//Set the language to what the user specified, and regenerate the application
		if (currentLang == "English") {
			Lang.setLang("en");
			MainWindow.regenerateAfterLanguageChange();
		}
		else if (currentLang == "Norsk") {
			Lang.setLang("no");
			MainWindow.regenerateAfterLanguageChange();
		}
		
	}
	
	/**
	 * Displays author names, with custom language prefix.
	 * @author Maciek
	 */
	public static void CommonAbout() {
		JOptionPane.showMessageDialog(
				null, Lang.getLang().getString("about.common") + "\nMaciej Piatkowski\nHåkon Legernæs\nThomas Strøm Grebstad");
	}
	
	/**
	 * Exits the application
	 * @author Maciek
	 */
	public static void CommonExit() {
		System.exit(0);
	}
	
}
