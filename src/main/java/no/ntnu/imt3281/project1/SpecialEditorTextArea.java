package no.ntnu.imt3281.project1;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * This class contains a JFrame object which is used as the GUI to change
 * special attributes of the TextArea component and is returned by itself in
 * the getSpecialEditor() method
 * 
 */

public class SpecialEditorTextArea extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	private TextArea textArea;
	
	private JSpinner spinnerRows;
	private JSpinner spinnerCols;
	private JCheckBox checkboxWrap;
	private JButton okButton;
	
	/**
	 * Constructor is responsible for initializing JFrame attributes and
	 * initializing and adding components to the JFrame layout
	 * @param object TextArea object which is to be modified
	 */
	public SpecialEditorTextArea(TextArea object)
	{
		this.textArea = object;
		
		this.setTitle(Lang.getLang().getString("specialeditor.title"));
		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		this.setResizable(false);
		this.getRootPane().setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		SpinnerNumberModel spinnerModelRows = 
				new SpinnerNumberModel(textArea.getTextRows(), 0, 100, 1);
		SpinnerNumberModel spinnerModelCols =
				new SpinnerNumberModel(textArea.getTextCols(), 0, 100, 1);
		
		spinnerRows = new JSpinner(spinnerModelRows);
		spinnerCols = new JSpinner(spinnerModelCols);
		
		checkboxWrap = new JCheckBox(Lang.getLang().getString("specialeditor.wrap"));
		checkboxWrap.setSelected(textArea.getWrap());
		
		okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textArea.setTextRows((int)spinnerRows.getValue());
				textArea.setTextCols((int)spinnerCols.getValue());
				if (checkboxWrap.isSelected())
					textArea.setWrap(true);
				else
					textArea.setWrap(false);
				dispose();
			}
		});
		
		add(new JLabel(Lang.getLang().getString("specialeditor.rows")));
		add(spinnerRows);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(new JLabel(Lang.getLang().getString("specialeditor.columns")));
		add(spinnerCols);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(checkboxWrap);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(okButton);
		setSize(400, 80);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
	}
}