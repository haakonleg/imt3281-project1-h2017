package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * This class extends JPanel and contains a JTable which uses GBLEDataModel as the table model.
 * It is responsible for visually representing the table model of the application and giving
 * the user the option to edit the attributes in the data model.
 */

public class GUITablePanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private JTable table;
	private JScrollPane scrollPane;
	
	private JPopupMenu popup;
	private JMenuItem pMoveUp;
	private JMenuItem pMoveDown;
	private JMenuItem pSpecialEditor;
	private JMenuItem pDelete;
	
	private JComboBox<Integer> sComboBox;
	private JComboBox<Integer> aComboBox;
	private JComboBox<String> tComboBox;
	
	/**
	 * Constructor for GUITablePanel. Responsible for setting up the table and adding
	 * components to it such as scrollpane, comboboxes and actionlisteners to the table panel
	 * 
	 * @param model The AbstractTableModel used for the JTable which is of class GBLEDataModel
	 */
	public GUITablePanel(GBLEDataModel model) 
	{
		this.setLayout(new GridLayout(1, 0));
		
		// Set up the JTable
		table = new JTable(model);
		table.setRowHeight(25);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getColumnModel().getColumn(1).setMinWidth(100);
		table.getColumnModel().getColumn(7).setMinWidth(100);
		table.getColumnModel().getColumn(8).setMinWidth(100);
		table.getColumnModel().getColumn(7).setCellRenderer(new AnchorRenderer());
		table.getColumnModel().getColumn(8).setCellRenderer(new ScalingRenderer());
		
		// Add scrollpane
		scrollPane = new JScrollPane(table);
		add(scrollPane);
		
		
		// Right-click popup menu
		popup = new JPopupMenu();
		PopupHandler popupHandler = new PopupHandler();
		PopupListener popupListener = new PopupListener();
		pMoveUp = new JMenuItem(Lang.getLang().getString("table.popup.moveup"));
		pMoveUp.addActionListener(popupHandler);
		pMoveDown = new JMenuItem(Lang.getLang().getString("table.popup.movedown"));
		pMoveDown.addActionListener(popupHandler);
		pSpecialEditor = new JMenuItem(Lang.getLang().getString("table.popup.special"));
		pSpecialEditor.addActionListener(popupHandler);
		pDelete = new JMenuItem(Lang.getLang().getString("table.popup.delete"));
		pDelete.addActionListener(popupHandler);
		popup.add(pMoveUp); popup.add(pMoveDown); popup.add(pSpecialEditor); popup.add(pDelete);
		popup.addPopupMenuListener(popupListener);
		table.setComponentPopupMenu(popup);
		
		// Anchor combo box
		aComboBox = new JComboBox<Integer>();
		aComboBox.addItem(java.awt.GridBagConstraints.CENTER);
		aComboBox.addItem(java.awt.GridBagConstraints.EAST);
		aComboBox.addItem(java.awt.GridBagConstraints.NORTH);
		aComboBox.addItem(java.awt.GridBagConstraints.NORTHEAST);
		aComboBox.addItem(java.awt.GridBagConstraints.NORTHWEST);
		aComboBox.addItem(java.awt.GridBagConstraints.SOUTH);
		aComboBox.addItem(java.awt.GridBagConstraints.SOUTHEAST);
		aComboBox.addItem(java.awt.GridBagConstraints.SOUTHWEST);
		aComboBox.addItem(java.awt.GridBagConstraints.WEST);
		aComboBox.setRenderer(new AnchorComboBoxRenderer());
		table.getColumnModel().getColumn(7).setCellEditor(new DefaultCellEditor(aComboBox));
		
		// Scaling combo box
		sComboBox = new JComboBox<Integer>();
		sComboBox.addItem(java.awt.GridBagConstraints.BOTH);
		sComboBox.addItem(java.awt.GridBagConstraints.HORIZONTAL);
		sComboBox.addItem(java.awt.GridBagConstraints.NONE);
		sComboBox.addItem(java.awt.GridBagConstraints.VERTICAL);
		sComboBox.setRenderer(new ScalingComboBoxRenderer());
		table.getColumnModel().getColumn(8).setCellEditor(new DefaultCellEditor(sComboBox));
		
		// Type combo box
		tComboBox = new JComboBox<String>();
		tComboBox.addItem("JTextField");
		tComboBox.addItem("JTextArea");
		tComboBox.addItem("JLabel");
		tComboBox.addItem("JButton");
		table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(tComboBox));
		
		// Select row on right click
		// Code taken from: https://stackoverflow.com/a/3558324
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				int r = table.rowAtPoint(e.getPoint());
				if (r >= 0 && r < table.getRowCount()) {
					table.setRowSelectionInterval(r, r);
				} else {
					table.clearSelection();
				}
			}
		});
	}
	
	/**
	 * This method implements PopupMenuListener and is used to enable/disable
	 * certain choices in the popup menu before showing it
	 *
	 */
	private class PopupListener implements PopupMenuListener {
		@Override
		public void popupMenuCanceled(PopupMenuEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
			// Get model
			GBLEDataModel model = (GBLEDataModel) GUITablePanel.this.table.getModel();
			
			// Get selected row
			int selectedRow = GUITablePanel.this.table.getSelectedRow();
			
			// Disable move up choice if first row is selected
			if (selectedRow == 0)
				GUITablePanel.this.pMoveUp.setEnabled(false);
			else
				GUITablePanel.this.pMoveUp.setEnabled(true);
			
			// Disable move down choice if last row is selected
			if (selectedRow == GUITablePanel.this.table.getRowCount() - 1)
				GUITablePanel.this.pMoveDown.setEnabled(false);
			else
				GUITablePanel.this.pMoveDown.setEnabled(true);
			// Disable special editor choice if getSpecialEditor returns null
			if(model.getComponent(selectedRow).getSpecialEditor() == null)
				GUITablePanel.this.pSpecialEditor.setEnabled(false);
			else
				GUITablePanel.this.pSpecialEditor.setEnabled(true);
		}
	}
	
	/**
	 * This method implements ActionListener and is used to handle
	 * menu choices in the popup menu. It uses functions from the
	 * GBLEDataModel class for the menu tasks
	 */
	private class PopupHandler implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
				
			// Get selected row
			int selectedRow = GUITablePanel.this.table.getSelectedRow();
			
			// Get model
			GBLEDataModel model = (GBLEDataModel) GUITablePanel.this.table.getModel();
			
			// Move up selected
			if (e.getActionCommand() == Lang.getLang().getString("table.popup.moveup"))
			{
				model.moveComponentUp(selectedRow);
			}
			// Move down selected
			else if (e.getActionCommand() == Lang.getLang().getString("table.popup.movedown"))
			{
				model.moveComponentDown(selectedRow);
			}
			// Special editor selected
			else if (e.getActionCommand() == Lang.getLang().getString("table.popup.special"))
			{
				JFrame editor = (JFrame) model.getComponent(selectedRow).getSpecialEditor();
				editor.setVisible(true);
			}
			// Delete selected
			else
			{
				model.removeComponent(selectedRow);
			}
		}
	}
	
	/**
	 * Custom renderer for the "Scaling" column. Used to show images instead of integers
	 * in the table cell. Creates a new JLabel and sets an image on it by using the 
	 * ImageIcon class, then returns it.
	 *
	 */
	private class ScalingRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object val, boolean isSelected, boolean hasFocus, int row, int col)
		{
			JLabel label = new JLabel();
			label.setOpaque(true);
			if (isSelected) {
				label.setBackground(table.getSelectionBackground());
				label.setForeground(table.getSelectionForeground());
			} else {
				label.setBackground(table.getBackground());
				label.setForeground(table.getForeground());
			}
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setVerticalAlignment(JLabel.CENTER);

			switch((int)val)
			{
				case java.awt.GridBagConstraints.BOTH:
					label.setIcon(new ImageIcon(getClass().getResource("/skaler_begge.png")));
					break;
				case java.awt.GridBagConstraints.HORIZONTAL:
					label.setIcon(new ImageIcon(getClass().getResource("/skaler_horisontalt.png")));
					break;
				case java.awt.GridBagConstraints.NONE:
					label.setIcon(new ImageIcon(getClass().getResource("/skaler_ingen.png")));
					break;
				case java.awt.GridBagConstraints.VERTICAL:
					label.setIcon(new ImageIcon(getClass().getResource("/skaler_vertikalt.png")));
					break;
			}
			return label;
		}
	}
	
	/**
	 * Custom renderer for the "Anchor" column. Used to show images instead of integers
	 * in the table cell. Creates a new JLabel and sets an image on it by using the 
	 * ImageIcon class, then returns it.
	 *
	 */
	private class AnchorRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object val, boolean isSelected, boolean hasFocus, int row, int col)
		{
			JLabel label = new JLabel();
			label.setOpaque(true);
			if (isSelected) {
				label.setBackground(table.getSelectionBackground());
				label.setForeground(table.getSelectionForeground());
			} else {
				label.setBackground(table.getBackground());
				label.setForeground(table.getForeground());
			}
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setVerticalAlignment(JLabel.CENTER);
			switch((int)val)
			{
				case java.awt.GridBagConstraints.CENTER:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_center.png")));
					break;
				case java.awt.GridBagConstraints.EAST:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_east.png")));
					break;
				case java.awt.GridBagConstraints.NORTH:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_north.png")));
					break;
				case java.awt.GridBagConstraints.NORTHEAST:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_northeast.png")));
					break;
				case java.awt.GridBagConstraints.NORTHWEST:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_northwest.png")));
					break;
				case java.awt.GridBagConstraints.SOUTH:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_south.png")));
					break;
				case java.awt.GridBagConstraints.SOUTHEAST:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_southeast.png")));
					break;
				case java.awt.GridBagConstraints.SOUTHWEST:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_southwest.png")));
					break;
				case java.awt.GridBagConstraints.WEST:
					label.setIcon(new ImageIcon(getClass().getResource("/anchor_west.png")));
					break;
			}
			return label;
		}
	}
	
	/**
	 * Custom renderer for the "Scaling" combobox cell editor. Used to show text instead
	 * of integers in the combobox. Extends JLabel and uses SetText to set the text in
	 * the menu list, then returns itself.
	 */
	private class ScalingComboBoxRenderer extends JLabel implements ListCellRenderer<Object>
	{
		private static final long serialVersionUID = 1L;
		
		public ScalingComboBoxRenderer()
		{
			this.setOpaque(true);
			this.setHorizontalAlignment(CENTER);
			this.setVerticalAlignment(CENTER);
		}
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object val, int index, boolean isSelected, boolean cellHasFocus)
		{
			if (isSelected)
			{
				this.setBackground(list.getSelectionBackground());
				this.setForeground(list.getSelectionForeground());
			}
			else
			{
				this.setBackground(list.getBackground());
				this.setForeground(list.getForeground());
			}
			switch((int)val)
			{
				case java.awt.GridBagConstraints.BOTH:
					this.setText(Lang.getLang().getString("table.combobox.both"));
					break;
				case java.awt.GridBagConstraints.HORIZONTAL:
					this.setText(Lang.getLang().getString("table.combobox.horizontal"));
					break;
				case java.awt.GridBagConstraints.NONE:
					this.setText(Lang.getLang().getString("table.combobox.none"));
					break;
				case java.awt.GridBagConstraints.VERTICAL:
					this.setText(Lang.getLang().getString("table.combobox.vertical"));
					break;
			}
			return this;
		}	
	}
	
	/**
	 * Custom renderer for the "Anchor" combobox cell editor. Used to show text instead
	 * of integers in the combobox. Extends JLabel and uses SetText to set the text in
	 * the menu list, then returns itself.
	 */
	private class AnchorComboBoxRenderer extends JLabel implements ListCellRenderer<Object>
	{
		private static final long serialVersionUID = 1L;

		public AnchorComboBoxRenderer()
		{
			this.setOpaque(true);
			this.setHorizontalAlignment(CENTER);
			this.setVerticalAlignment(CENTER);
		}
		
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object val, int index, boolean isSelected, boolean cellHasFocus) {
			if (isSelected)
			{
				this.setBackground(list.getSelectionBackground());
				this.setForeground(list.getSelectionForeground());
			}
			else
			{
				this.setBackground(list.getBackground());
				this.setForeground(list.getForeground());
			}
			switch((int)val)
			{
				case java.awt.GridBagConstraints.CENTER:
					this.setText(Lang.getLang().getString("table.combobox.center"));
					break;
				case java.awt.GridBagConstraints.EAST:
					this.setText(Lang.getLang().getString("table.combobox.east"));
					break;
				case java.awt.GridBagConstraints.NORTH:
					this.setText(Lang.getLang().getString("table.combobox.north"));
					break;
				case java.awt.GridBagConstraints.NORTHEAST:
					this.setText(Lang.getLang().getString("table.combobox.northeast"));
					break;
				case java.awt.GridBagConstraints.NORTHWEST:
					this.setText(Lang.getLang().getString("table.combobox.northwest"));
					break;
				case java.awt.GridBagConstraints.SOUTH:
					this.setText(Lang.getLang().getString("table.combobox.south"));
					break;
				case java.awt.GridBagConstraints.SOUTHEAST:
					this.setText(Lang.getLang().getString("table.combobox.southeast"));
					break;
				case java.awt.GridBagConstraints.SOUTHWEST:
					this.setText(Lang.getLang().getString("table.combobox.southwest"));
					break;
				case java.awt.GridBagConstraints.WEST:
					this.setText(Lang.getLang().getString("table.combobox.west"));
					break;
			}
			return this;
		}
	}
	
	/**
	 * Returns chosen row to the caller
	 * @return chosen row
	 */
	public int GetSelectedRow() {
		return table.getSelectedRow();
	}
	
	/**
	 * Regenerates table structure after changes in the table, caused by the language change
	 */
	public void regenerateAfterLanguageChange() {
		table.setRowHeight(25);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getColumnModel().getColumn(1).setMinWidth(100);
		table.getColumnModel().getColumn(7).setMinWidth(100);
		table.getColumnModel().getColumn(8).setMinWidth(100);
		table.getColumnModel().getColumn(7).setCellRenderer(new AnchorRenderer());
		table.getColumnModel().getColumn(8).setCellRenderer(new ScalingRenderer());
		table.getColumnModel().getColumn(7).setCellEditor(new DefaultCellEditor(aComboBox));
		table.getColumnModel().getColumn(8).setCellEditor(new DefaultCellEditor(sComboBox));
		table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(tComboBox));
	}
	
}