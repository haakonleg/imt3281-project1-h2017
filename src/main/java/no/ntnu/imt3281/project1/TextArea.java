package no.ntnu.imt3281.project1;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The class TextArea extends BaseComponent and represents a javax.swing.JTextArea object
 * @author Hakkon
 *
 */

public class TextArea extends BaseComponent
{
	private static final long serialVersionUID = 1L;

	private int textRows;
	private int textCols;
	private boolean wrap;
	
	/**
	 * Constructor creates a new TextArea object and initializes the attributes to default values
	 */
	public TextArea()
	{
		this.textRows = 0;
		this.textCols = 0;
	}
	
	/**
	 * Constructor creates a new TextArea object based upon another BaseComponent object and copies the attributes
	 * to the new TextArea object
	 * @param component BaseComponent object to copy
	 */
	public TextArea(BaseComponent component)
	{
		super(component);
		// If component is another TextArea object copy its attributes
		if (component instanceof TextArea) {
			this.textRows = ((TextArea)component).getTextRows();
			this.textCols = ((TextArea)component).getTextCols();
		} else {
			this.textRows = 0;
			this.textCols = 0;
		}
	}
	
	/**
	 * This method returns the textRows attribute of this TextArea component
	 * @return Int the textRows attribute of this object
	 */
	public int getTextRows()
	{
		return this.textRows;
	}
	
	/**
	 * This method sets the textRows attribute of this TextArea component
	 * @param n Int the integer to set textRows to
	 */
	public void setTextRows(int n)
	{
		this.textRows = n;
	}
	
	/**
	 * This method returns the textCols attribute of this TextArea component
	 * @return Int the textCols attribute of this object
	 */
	public int getTextCols()
	{
		return this.textCols;
	}
	
	/**
	 * This method sets the textCols attribute of this TextArea component
	 * @param n Int the integer to set textCols to
	 */
	public void setTextCols(int n)
	{
		this.textCols = n;
	}
	
	/**
	 * This method returns the wrap attribute of this TextArea component
	 * @return Int the wrap attribute of this object
	 */
	public boolean getWrap()
	{
		return this.wrap;
	}
	
	/**
	 * This method sets the wrap attribute of this TextArea component
	 * @param w The boolean to set wrap attribute to
	 */
	public void setWrap(boolean w)
	{
		this.wrap = w;
	}
	
	/**
	 * This method returns the string for the definition of this swing component in Java code
	 * @return String java code definition
	 */
	@Override
	public String getDefinition()
	{
		return "\tJTextArea " + this.variableName + " = new JTextArea(\"" + this.text + "\", " + Integer.toString(this.textRows) + ", " + Integer.toString(this.textCols) + ");\n";
	}
	
	/**
	 * This method returns a JFrame window which contains the GUI to change
	 * special attributes of this component such as textCols, textRows and wrap.
	 * @return JFrame GUI which can be used to change special attributes
	 */
	@Override
	public java.awt.Component getSpecialEditor()
	{
		return new SpecialEditorTextArea(this);
	}
	
	/**
	 * This method returns the Java layout code used by GridBagLayout layout manager
	 * @return String with Java layout code
	 */
	@Override
	public String getLayoutCode()
	{
		return super.getLayoutCode() + "\t\t" + this.variableName + ".setWrapStyleWord(" + Boolean.toString(this.wrap) + ");\n";
	}
}